Source: soci
Section: libs
Priority: optional
Maintainer: William Blough <bblough@debian.org>
Build-Depends: cmake,
               debhelper-compat (= 13),
               default-libmysqlclient-dev,
               firebird-dev,
               libboost-date-time-dev,
               libboost-dev,
               libpq-dev,
               libsqlite3-dev,
               unixodbc-dev
Standards-Version: 4.6.1
Homepage: http://soci.sourceforge.net/
Vcs-Browser: https://salsa.debian.org/bblough/soci
Vcs-Git: https://salsa.debian.org/bblough/soci.git

Package: libsoci-core4.0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: C++ Database Access Library
 Database access library for C++ that makes the illusion of
 embedding SQL queries in the regular C++ code, staying entirely
 within Standard C++.
 .
 This package provides the shared library.

Package: libsoci-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libsoci-core4.0 (= ${binary:Version}),
         libsoci-firebird4.0 (= ${binary:Version}),
         libsoci-mysql4.0 (= ${binary:Version}),
         libsoci-odbc4.0 (= ${binary:Version}),
         libsoci-postgresql4.0 (= ${binary:Version}),
         libsoci-sqlite3-4.0 (= ${binary:Version}),
         ${misc:Depends}
Description: C++ Database Access Library (devel)
 Database access library for C++ that makes the illusion of
 embedding SQL queries in the regular C++ code, staying entirely
 within Standard C++.
 .
 This package provides the development headers.

Package: libsoci-mysql4.0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: C++ Database Access Library (MySQL backend)
 Database access library for C++ that makes the illusion of
 embedding SQL queries in the regular C++ code, staying entirely
 within Standard C++.
 .
 This package provides the MySQL backend.

Package: libsoci-sqlite3-4.0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: C++ Database Access Library (SQLite3 backend)
 Database access library for C++ that makes the illusion of
 embedding SQL queries in the regular C++ code, staying entirely
 within Standard C++.
 .
 This package provides the SQLite3 backend.

Package: libsoci-postgresql4.0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: C++ Database Access Library (PostgreSQL backend)
 Database access library for C++ that makes the illusion of
 embedding SQL queries in the regular C++ code, staying entirely
 within Standard C++.
 .
 This package provides the PostgreSQL backend.

Package: libsoci-firebird4.0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: C++ Database Access Library (Firebird backend)
 Database access library for C++ that makes the illusion of
 embedding SQL queries in the regular C++ code,  staying entirely
 within Standard C++.
 .
 This package provides the Firebird backend.

Package: libsoci-odbc4.0
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${misc:Depends}, ${shlibs:Depends}
Description: C++ Database Access Library (ODBC backend)
 Database access library for C++ that makes the illusion of
 embedding SQL queries in the regular C++ code,  staying entirely
 within Standard C++.
 .
 This package provides the ODBC backend.
